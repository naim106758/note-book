import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../models/DatabaseHelper.dart';
import '../models/user.dart';
import '../utill_page.dart';
import 'custom_button.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({super.key});

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextEditingController? userMailTextCtl;
  TextEditingController? passwordTextCtl;
  TextEditingController? firstNameTextCtl;
  GlobalKey<FormState>? formKey;
  DatabaseHelper? db;
  bool isVisible = true;

  @override
  void initState() {
    super.initState();
    userMailTextCtl = TextEditingController();
    passwordTextCtl = TextEditingController();
    firstNameTextCtl = TextEditingController();
    formKey = GlobalKey<FormState>();
    db = DatabaseHelper();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 38, right: 38),
              child: Form(
                key: formKey,
                child: Column(
                  children: [
                    //Username
                    Container(
                      height: 70,
                      alignment: Alignment.centerLeft,
                      child: TextFormField(
                        onChanged: (value){
                          setState(() {

                          });
                        },
                        keyboardType: TextInputType.text,
                        obscureText: false,
                        controller: firstNameTextCtl,
                        enabled: true,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Please Provide FirstName";
                          } else {
                            return null;
                          }
                        },
                        decoration:InputDecoration(
                          hintText: "Enter First Name",
                          prefixIcon: Icon(Icons.person),
                          labelText: "Name",
                          suffixIcon:firstNameTextCtl!.text.isEmpty?Text(" "): GestureDetector(
                            onTap: (){
                              firstNameTextCtl?.clear();
                            },
                            child: Icon(Icons.clear),),
                          border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10))),
                          contentPadding: EdgeInsets.only(
                              left: 11, right: 3, top: 14, bottom: 14),
                          errorStyle: TextStyle(fontSize: 11, height: 0.2),
                          errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(width: 4, color: Colors.red),
                          ),
                          focusedErrorBorder: OutlineInputBorder(
                            borderSide: BorderSide(width: 5, color: Colors.red),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: 70,
                      alignment: Alignment.centerLeft,
                      child: TextFormField(
                        onChanged: (value){
                          setState(() {

                          });
                        },
                        keyboardType: TextInputType.text,
                        obscureText: false,
                        controller: userMailTextCtl,
                        enabled: true,
                        validator: (value) {
                          if (value == null ||
                              value.isEmpty ||
                              Utill.isEmailFormatNotValid(value) == true) {
                            return "Please Provide UserMail";
                          } else {
                            return null;
                          }
                        },
                        decoration: InputDecoration(
                          hintText: "Enter user Mail",
                          prefixIcon: Icon(Icons.mail),
                          labelText: "Mail",
                          suffixIcon:userMailTextCtl!.text.isEmpty?Text(" ") :GestureDetector(
                            onTap: (){
                              firstNameTextCtl?.clear();
                            },
                            child: Icon(Icons.clear),),
                          border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10))),
                          contentPadding: EdgeInsets.only(
                              left: 11, right: 3, top: 14, bottom: 14),
                          errorStyle: TextStyle(fontSize: 11, height: 0.2),
                          errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(width: 4, color: Colors.red),
                          ),
                          focusedErrorBorder: OutlineInputBorder(
                            borderSide: BorderSide(width: 5, color: Colors.red),
                          ),
                        ),
                      ),
                    ),
                    //password
                    Container(
                      height: 70,
                      alignment: Alignment.centerLeft,
                      child: TextFormField(
                        keyboardType: TextInputType.text,
                        obscureText: isVisible,
                        controller: passwordTextCtl,
                        enabled: true,
                        validator: (value) {
                          if (value == null ||
                              value.isEmpty ||
                              value.length < 4) {
                            return "Password must be 5 digit long";
                          } else {
                            return null;
                          }
                        },
                        decoration:InputDecoration(
                          hintText: "Enter password",
                          prefixIcon: Icon(Icons.lock),
                          labelText: "password",
                          suffixIcon:GestureDetector(
                            onTap: (){
                              isVisible=!isVisible;
                              setState(() {

                              });
                      },
                        child: Icon(Icons.visibility),),
                          border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10))),
                          contentPadding: EdgeInsets.only(
                              left: 11, right: 3, top: 14, bottom: 14),
                          errorStyle: TextStyle(fontSize: 11, height: 0.2),
                          errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(width: 4, color: Colors.red),
                          ),
                          focusedErrorBorder: OutlineInputBorder(
                            borderSide: BorderSide(width: 5, color: Colors.red),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    CustomButton(
                      text: "REGISTER",
                      onPressed: () async {
                         if (formKey!.currentState!.validate()) {
                          User user = User(
                              name: firstNameTextCtl!.text,
                              username: userMailTextCtl!.text,
                              password: passwordTextCtl!.text);

                          bool status =
                          await db!.isExist(userMailTextCtl!.text);
                          if (status == true) {

                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                content: Text("username already exist")));
                          } else {
                             int userSave = await db!.userRegistration(user);
                            if (userSave > 0) {
                               ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                      content: Text(" Successfuly Registerd")));
                            }
                            Navigator.pop(context);
                          }
                        }
                      },
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Center(
                      child: InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: const Padding(
                          padding:
                          EdgeInsets.only(bottom: 25, left: 5, right: 5),
                          child: Text("Already registered, LogIN Now"),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
