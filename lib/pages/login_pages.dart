import 'package:flutter/material.dart';
import 'package:notebook/memory_management.dart';
import '../models/DatabaseHelper.dart';
import '../models/user.dart';
import '../utill_page.dart';
import 'Register_page.dart';
import 'custom_button.dart';
import 'home_page.dart';

class LogInPages extends StatefulWidget {
  const LogInPages({Key? key}) : super(key: key);

  @override
  State<LogInPages> createState() => _LogInPagesState();
}

class _LogInPagesState extends State<LogInPages> {
  TextEditingController? userMailTextCtl;
  TextEditingController? passwordTextCtl;
  GlobalKey<FormState>? formKey;
  DatabaseHelper? db;
  bool isVisible = true;

  @override
  void initState() {
    super.initState();
    userMailTextCtl = TextEditingController();
    passwordTextCtl = TextEditingController();
    formKey = GlobalKey<FormState>();
    db = DatabaseHelper();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.only(left: 38, right: 38,),
              child: Form(
                key: formKey,
                child: Column(
                  children: [
                    //Username
                    Container(
                      height:70,
                      alignment: Alignment.centerLeft,
                      child: TextFormField(
                        onChanged: (value){
                          setState(() {

                          });
                        },
                        keyboardType: TextInputType.text,
                        obscureText: false,
                        controller: userMailTextCtl,
                        enabled: true,
                        validator: (value) {
                          if (value == null ||
                              value.isEmpty ||
                              Utill.isEmailFormatNotValid(value) == true) {
                            return "Please Provide UserEmail";
                          } else {
                            return null;
                          }
                        },
                        decoration: InputDecoration(
                          hintText: "Enter UserEmail",
                          prefixIcon: Icon(Icons.mail),
                          labelText: "Email",
                          suffixIcon:userMailTextCtl!.text.isEmpty?Text(" "): GestureDetector(
                              onTap: (){
                                userMailTextCtl?.clear();
                              },
                              child: Icon(Icons.close)),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(15)
                          ),
                          contentPadding: EdgeInsets.only(
                              left: 11, right: 3, top: 14, bottom: 14),
                          errorStyle: TextStyle(fontSize: 11, height: 0.2),
                          errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(width:3, color: Colors.red),
                          ),
                          focusedErrorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(width:4, color: Colors.red),
                          ),
                        ),
                      ),
                    ),
                    //password
                    Container(
                      height: 70,
                      alignment: Alignment.centerLeft,
                      child: TextFormField(
                        keyboardType: TextInputType.text,
                        obscureText: isVisible,
                        controller: passwordTextCtl,
                        enabled: true,
                        validator: (value) {
                          if (value == null ||
                              value.isEmpty ||
                              value.length < 4) {
                            return "Password must be 5 digit long";
                          } else {
                            return null;
                          }
                        },
                        decoration: InputDecoration(
                          hintText: "Enter password",
                          labelText: "password",
                          prefixIcon: Icon(Icons.lock),
                          suffixIcon:GestureDetector(
                            onTap: (){
                              isVisible=!isVisible;
                              setState(() {

                              });
                            },
                            child: Icon(Icons.visibility),),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)
                          ),
                          contentPadding: EdgeInsets.only(
                              left: 11, right: 3, top: 14, bottom: 14),
                          errorStyle: TextStyle(fontSize: 11, height: 0.2),
                          errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                             borderSide: BorderSide(width: 4, color: Colors.red),
                          ),
                          focusedErrorBorder: OutlineInputBorder(
                            borderSide: BorderSide(width: 5, color: Colors.red),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    CustomButton(
                      text: "LogIn",
                      onPressed: () async {
                        if (formKey!.currentState!.validate()) {
                          List<User> userList = await db!.fetchUserList(
                              userMailTextCtl!.text, passwordTextCtl!.text);
                          if (userList.isEmpty) {
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                content: Text(
                                    "username or password are nor exist")));
                          } else {
                            MemoryManagement.setLoggedIn(true);
                            Navigator.pushAndRemoveUntil(context,
                                MaterialPageRoute(builder: (context) {
                              return HomePage();
                            }), (route) => false);
                          }
                        }
                      },
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Center(
                      child: InkWell(
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>RegisterPage()));
                        },
                        child: Padding(
                          padding:
                              EdgeInsets.only(bottom: 25, left: 5, right: 5),
                          child: Text("New To App? Register Now"),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
