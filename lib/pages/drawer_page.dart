import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:notebook/pages/login_pages.dart';

import '../constants/appconstants.dart';
import '../memory_management.dart';
import '../models/DatabaseHelper.dart';

class DrawerPage extends StatefulWidget {
  const DrawerPage({Key? key}) : super(key: key);

  @override
  State<DrawerPage> createState() => _DrawerPageState();
}

class _DrawerPageState extends State<DrawerPage> {
  DatabaseHelper? db;
  @override
  void initState() {
    super.initState();
    db = DatabaseHelper();
  }

  @override

  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          UserAccountsDrawerHeader(
              currentAccountPicture: CircleAvatar(
                  child:Text("A")
                // Image.asset("name"),
              ),
              otherAccountsPictures: [
                CircleAvatar(
                  child: Text("A"),
                ),
                CircleAvatar(
                  child: Text("A"
                      "H"),
                ),
              ],
              accountName: Text("MD.Naiym Hosen"),
              accountEmail: Text("naiymhoshen@gmail.com")),
          ListTile(
            title: const Text(' Delete Account'),
            leading: Icon(Icons.delete),

            onTap: () async {
              await db!.deleteTable(AppConstants.tableName);
              await db!.deleteTable(AppConstants.userTableName);
              MemoryManagement.setLoggedIn(false);
              Navigator.pushAndRemoveUntil(context,
                  MaterialPageRoute(builder: (context) {
                    return LogInPages();
                  }), (route) => false);
            },
          ),
          ListTile(
            title: const Text('LogOut'),
            leading: Icon(Icons.logout_rounded),
            onTap: () {
              MemoryManagement.setLoggedIn(false);
              Navigator.pushAndRemoveUntil(context,
                  MaterialPageRoute(builder: (context) {
                    return LogInPages();
                  }), (route) => false);
            },
          ),
          ListTile(
            title: Text('Home'),
            leading: Icon(Icons.home),
            trailing: Icon(Icons.edit),
            onTap: () {
            },
          ),
        ],
        ),
    );
  }
}
