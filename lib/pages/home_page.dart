import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../models/DatabaseHelper.dart';
import '../models/note_book.dart';
import '../utill_page.dart';
import 'drawer_page.dart';
import 'note_add_pages.dart';
class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<NoteBook>? nameList;
  DatabaseHelper? _databaseHelper;
  List<NoteBook>? noteList;
  List<NoteBook>? noteListContainer;

  @override
  void initState() {

    super.initState();
    nameList = [];
    noteList = [];
    noteListContainer = [];
    _databaseHelper = DatabaseHelper();
    fetchNoteList();
  }


  void fetchNoteList() async {
    try {
      noteListContainer!.clear();
      noteListContainer = await _databaseHelper!.fetchNoteBookList();
      if (noteListContainer!.isNotEmpty) {
        setState(() {
          noteList = noteListContainer;
        });
      }
    } catch (error) {}
  }

  void filterSearchResult(String? query){
    if(query!.isEmpty){
      setState(() {
        noteList = noteListContainer;
      });
    }else{
      List<NoteBook> mList= [];
      for(NoteBook book in noteListContainer!){
        if(book.title!.toLowerCase().contains(query.toLowerCase()) ){
          mList.add(book);
        }
      }
      setState(() {
        noteList = mList;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        floatingActionButton: Container(
          margin: EdgeInsets.only(bottom: 20, right: 20),
          child: FloatingActionButton(
            onPressed: () async {
              bool? isAdded = await Navigator.push(context,
                  MaterialPageRoute(builder: (context) {
                    return NoteAddPage();

                  }));
              if (isAdded != null) {
                if (isAdded == true) {
                  noteList!.clear();
                  fetchNoteList();
                }
              }
            },
            elevation: 5,
            child: Icon(Icons.add),
          ),
        ),
        appBar: AppBar(
          title: Text(
            "NoteBook (${noteList!.length})",
            style: TextStyle(fontSize: 25),
          ),
          centerTitle: true,
        ),
        drawer: DrawerPage(),
        body: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.center,

            children: [
              Row(
                children: [
                  Icon(
                    Icons.menu_book,
                    size: 30,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Hello",
                    style:
                    TextStyle(fontSize: 20, fontWeight: FontWeight.normal),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Habib",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    Utill.greeting(),
                    style:
                    TextStyle(fontSize: 20, fontWeight: FontWeight.normal),
                  ),
                ],
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                margin: EdgeInsets.all(10),
                child: TextField(
                  onChanged: (String newchar) {
                    filterSearchResult(newchar);
                  },
                  decoration: InputDecoration(
                      hintText: "Search by name",
                      hintStyle: TextStyle(color: Colors.grey),
                      // prefix: Icon(Icons.search,color: Colors.grey,)
                      fillColor: Colors.grey.shade200,
                      filled: true,
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.black,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(4.0)),
                      ),
                      border: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.grey.shade400,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(10)))),
                ),
              ),
              Expanded(
                child: ListView.separated(
                    separatorBuilder: (context, index) {
                      return SizedBox(
                        height: 10,
                      );
                    },
                    itemCount: noteList!.length,
                    itemBuilder: (context, index) {
                      NoteBook noteBook = noteList![index];
                      return Padding(
                        padding: EdgeInsets.only(left: 10, right: 10),
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              color: Colors.white,
                              border: Border.all(
                                  width: 2.0, color: Colors.blueAccent)),
                          child: Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    children: [
                                      Text(
                                        noteBook.title!,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18),
                                      ),
                                      Text(
                                        noteBook.content!,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.normal,
                                            fontSize: 18),
                                      ),
                                      Text(
                                        noteBook.date!,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.normal,
                                            fontSize: 18),
                                      ),
                                    ],
                                  ),
                                ),
                                PopupMenuButton<String>(
                                    padding: EdgeInsets.zero,
                                    onSelected: (value) {},
                                    icon: Icon(Icons.more_vert),
                                    itemBuilder: (context) {
                                      return <PopupMenuEntry<String>>[
                                        PopupMenuItem(
                                          value: "edit",
                                          child: ListTile(
                                            leading: Icon(Icons.edit),
                                            title: Text("Update"),
                                          ),
                                        ),
                                        PopupMenuItem(
                                          value: "delete",
                                          child: ListTile(
                                            leading: Icon(Icons.delete),
                                            title: Text("Delete"),
                                          ),
                                        )
                                      ];
                                    })
                              ],
                            ),
                          ),
                        ),
                      );
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
